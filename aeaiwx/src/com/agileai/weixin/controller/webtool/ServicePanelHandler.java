package com.agileai.weixin.controller.webtool;

import java.util.UUID;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.weixin.model.Constans.Configs;
import com.agileai.weixin.tool.SecurityAuthHelper;

public class ServicePanelHandler extends SimpleHandler{
	public ServicePanelHandler(){
		super();
	}
	@PageAction
	public ViewRenderer init(DataParam param){
		String responseText = FAIL;
		String nonceStr = UUID.randomUUID().toString();
		String timestamp = String.valueOf(System.currentTimeMillis()/1000);
		String url = param.get("url");
		try {
			JSONObject jsonObject = new JSONObject();
			String accessToken = SecurityAuthHelper.getAccessToken();
			String jsapi_ticket = SecurityAuthHelper.getJsapiTicket(accessToken);
			String signature = SecurityAuthHelper.signJsApi(jsapi_ticket, nonceStr, timestamp, url);
			
			jsonObject.put("appId", Configs.APPID);
			jsonObject.put("timestamp", timestamp);
			jsonObject.put("nonceStr", nonceStr);
			jsonObject.put("signature",signature);
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
