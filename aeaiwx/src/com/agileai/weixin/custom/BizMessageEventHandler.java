package com.agileai.weixin.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.agileai.weixin.core.MessageEventHandler;
import com.agileai.weixin.model.TextMessage;
import com.agileai.weixin.model.Constans.ReqType;
import com.agileai.weixin.tool.MessageBuilder;

public class BizMessageEventHandler extends MessageEventHandler {
	protected static List<String> RecognizableText = new ArrayList<String>();
	
	public BizMessageEventHandler(){
		if (RecognizableText.isEmpty()){
			RecognizableText.add("1");
			RecognizableText.add("2");
			RecognizableText.add("3");
			RecognizableText.add("4");
			RecognizableText.add("5");
		}
	}
	
	public String handleSubscribe(Map<String, String> requestMap){
		String result = null;
		
		String fromUserName = requestMap.get("FromUserName");
		String toUserName = requestMap.get("ToUserName");
		
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(ReqType.TEXT);
		textMessage.setFuncFlag(0);
		
		StringBuffer contentMsg = new StringBuffer();  
		contentMsg.append("多谢关注！沈阳数通畅联软件技术有限公司是耕耘于软件集成领域的专业技术团队，以“分享SOA平台软件、传递敏捷集成机制”为使命，希望以自身所长，为客户和伙伴提供从数据层、服务层、应用层、流程层、交互层全方位的产品和技术解决方案。");
		contentMsg.append("欢迎访问<a href=\"http://www.agileai.com\">手机网站</a>。");  
		
		textMessage.setContent(contentMsg.toString());
		result = MessageBuilder.textMessageToXml(textMessage);
		
		return result;
	}	
	
	public String handleTextMessage(Map<String, String> requestMap){
		String result = null;
		String content = requestMap.get("Content");
		
		if (RecognizableText.contains(content)){
			String fromUserName = requestMap.get("FromUserName");
			String toUserName = requestMap.get("ToUserName");
			TextMessage textMessage = new TextMessage();
			textMessage.setToUserName(fromUserName);
			textMessage.setFromUserName(toUserName);
			textMessage.setCreateTime(new Date().getTime());
			textMessage.setMsgType(ReqType.TEXT);
			textMessage.setFuncFlag(0);
			StringBuffer contentMsg = new StringBuffer();  
			contentMsg.append("亲，您的输入是").append(content);  
			textMessage.setContent(contentMsg.toString());
			result = MessageBuilder.textMessageToXml(textMessage);
		}else{
			String fromUserName = requestMap.get("FromUserName");
			String toUserName = requestMap.get("ToUserName");
			TextMessage textMessage = new TextMessage();
			textMessage.setToUserName(fromUserName);
			textMessage.setFromUserName(toUserName);
			textMessage.setCreateTime(new Date().getTime());
			textMessage.setMsgType(ReqType.TEXT);
			textMessage.setFuncFlag(0);
			
			StringBuffer contentMsg = new StringBuffer();  
			contentMsg.append("亲，您的输入不能被识别:)");  

			textMessage.setContent(contentMsg.toString());
			result = MessageBuilder.textMessageToXml(textMessage);
		}
		return result;
	}
	
	public String handleLocationEvent(Map<String, String> requestMap){
		String result = null;

		String openId = requestMap.get("FromUserName");
		double latitude = Double.parseDouble(requestMap.get("Latitude"));
		double longitude = Double.parseDouble(requestMap.get("Longitude"));
		double precision = Double.parseDouble(requestMap.get("Precision"));
		
		HashMap<String,Object> row = new HashMap<String,Object>();
		row.put("Latitude",latitude);
		row.put("Longitude",longitude);
		row.put("Precision",precision);
		row.put("receiveTime",new Date());
		LocationCache.put(openId, row);	
		
		return result;
	}	
	
	public String handleMenuClickEvent(String eventKey,Map<String, String> requestMap){
		String result = null;
		
		if ("MyWork".equals(eventKey)){
			String fromUserName = requestMap.get("FromUserName");
			String toUserName = requestMap.get("ToUserName");
			TextMessage textMessage = new TextMessage();
			textMessage.setToUserName(fromUserName);
			textMessage.setFromUserName(toUserName);
			textMessage.setCreateTime(new Date().getTime());
			textMessage.setMsgType(ReqType.TEXT);
			textMessage.setFuncFlag(0);
			
			StringBuffer contentMsg = new StringBuffer();  
			contentMsg.append("我的工作包括所有该用户的工作相关功能，如：我的待办、我的待阅、我的信息、我的日程、我的客户、我的订阅等，目前正在集成中……").append("\n");  

			textMessage.setContent(contentMsg.toString());
			result = MessageBuilder.textMessageToXml(textMessage);
		}
		return result;
	}		
}
